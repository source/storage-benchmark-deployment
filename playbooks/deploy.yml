- hosts: terraformed
  become: yes
  tasks:
  - name: add ssh key to root
    authorized_key:
      key: "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC1gRhavvtL92IIuXdVujwVnptT2aDlJafaDNrr7AVo2Eboc7MAlQC+9UHDpMqVhmLL3J9OFR7vSFMYjsRoREIB5wxsoTOQy5D2kZwbu28dMy7ZEM+gNFyQaGucngTSj0d3m80RVBYOMDupaXtPuw0fEzRKmHMOr3zMhCK8djbdMKIWnPMA2mxGjiCY22jd7k3jlGti3iXjZ+Mrl1VGKuOXiEpHsDNoQfL289odkP4nWQA95tS6Lwi2gUtSLmhu9QTfIoFVFwdGT8jLa3ql3Ia45otCDTH2SrI9ZYoUhmTDNwcVhhkpfCb8tbsU0zD6qSTn5EWHpv6+56oa3nLutgF1 azure@desktop5"
      user: root

  - name: add host to /etc/hosts
    # Fixes some quirks, like https://stackoverflow.com/q/7496640/539465
    # or sudo complaining
    lineinfile:
      path: /etc/hosts
      regexp: ".*\\b{{ansible_hostname}}\\b.*"
      line: "127.0.0.1 {{ansible_hostname}}"

  - name: install APT dependencies
    apt:
      name: [
        xfsprogs, python-pkg-resources, rsync, htop, iotop, pv, pigz,
        parallel, mdadm]

  - name: configure RAID
    include_role:
      name: mdadm
    vars:
      mdadm_arrays:
      - name: md0
        devices: "{{data_disks}}"
        filesystem: xfs
        level: 0
        mountpoint: "{{data_mountpoint}}"
        state: present
        opts: noatime
    when: "data_disks|length > 1"

  - name: partition data disks
    parted:
      device: "{{data_disks[0]}}"
      number: 1
      state: present
    when: "data_disks|length <= 1"
  - name: make data filesystem
    filesystem:
      device: "{{data_disks[0]}}1"
      fstype: xfs
    when: "data_disks|length <= 1"
  - name: mount data disk
    mount:
      path: "{{data_mountpoint}}"
      src: "{{data_disks[0]}}1"
      fstype: xfs
      state: mounted
    when: "data_disks|length <= 1"

- hosts: pgsql-servers
  become: yes
  tasks:
  - name: add pgsql apt repo key
    apt_key:
      url: "https://www.postgresql.org/media/keys/ACCC4CF8.asc"
  
  - name: add pgsql apt repo
    apt_repository:
      repo: "deb http://apt.postgresql.org/pub/repos/apt/ stretch-pgdg main"
      filename: "pgsql.sources.list"

  - name: install pgsql
    apt:
      name: [postgresql, python-psycopg2]

  - name: make pgsql listen on all addresses
    lineinfile:
      line: "listen_addresses = '*'"
      # TODO: autodetect installed version
      path: /etc/postgresql/11/main/postgresql.conf
      regexp: "^listen_addresses *=.*"
    notify: restart pgsql

  - name: allow pgsql connections from clients
    lineinfile:
      line: "host  all  all {{hostvars[item].ansible_host}}/24 md5"
      # TODO: autodetect installed version
      path: /etc/postgresql/11/main/pg_hba.conf
    with_items: "{{groups['clients']}}"
    notify: restart pgsql

  - name: initialize pgsql db
    become: true
    become_user: postgres
    postgresql_db:
      name: softwareheritage-test

  - name: add pgsql user
    become: true
    become_user: postgres
    postgresql_user:
      name: swh
      password: testpassword
      db: softwareheritage-test

  - name: copy SQL init files
    copy:
      src: /home/dev/swh-environment/swh-storage/swh/storage/sql/
      dest: sql/

  - name: initialize pgsql db
    become: true
    become_user: postgres
    shell: >
      cat sql/10* sql/20* sql/30* sql/40* sql/50* sql/60*
      | psql softwareheritage-test
    register: psql_result
    failed_when: "psql_result.rc != 0 or 'does not exist' in psql_result.stderr"

  - name: grant permissions
    become: yes
    become_user: postgres
    shell: psql softwareheritage-test
    args:
      stdin: |
        GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO swh;
        GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO swh;
    register: psql_result
    failed_when: "psql_result.rc != 0 or 'ERROR' in psql_result.stderr"

  handlers:
  - name: restart pgsql
    service:
      name: postgresql
      state: restarted

- hosts: cassandra-servers
  become: yes
  tasks:
  - name: add Cassandra apt repo key
    apt_key:
      url: "https://www.apache.org/dist/cassandra/KEYS"
  
  - name: add Cassandra apt repo
    apt_repository:
      repo: "deb http://www.apache.org/dist/cassandra/debian 311x main"
      filename: "cassandra.sources.list"
  
  - name: install Cassandra package
    apt:
      name: cassandra

  - name: chown data disk
    file:
      path: /mnt/cassandra_data
      owner: cassandra
      group: cassandra
      state: directory

  - name: configure cassandra
    template:
      src: cassandra.yaml
      dest: /etc/cassandra/cassandra.yaml
    vars:
      data_dir: /mnt/cassandra_data
    notify: restart cassandra

  - name: add cassandra systemd unit
    # The Cassandra package uses a sysv init script, and
    # systemd-sysv-generator assumes it's Type=simple
    copy:
      dest: /etc/systemd/system/cassandra.service
      content: |
        [Unit]
        Description=Cassandra
        After=network-online.target

        [Service]
        Type=forking
        User=cassandra
        Group=cassandra
        ExecStart=/usr/sbin/cassandra

        [Install]
        WantedBy=multi-user.target
    notify: daemon-reload

  - name: enable cassandra system unit
    systemd:
      name: cassandra.service
      enabled: yes

  handlers:
  - name: daemon-reload
    systemd:
      daemon_reload: yes
    notify: restart cassandra

  - name: restart cassandra
    service:
      name: cassandra
      state: restarted
  
- hosts: clients
  become: yes
  tasks:
  - name: chown data disk
    file:
      path: /mnt/dataset
      owner: vlorentz
      group: vlorentz
      state: directory

  - name: install APT dependencies
    apt:
      name: [
        python3, python3-dev, python3-pip, libpq-dev,
        libsvn-dev, libsystemd-dev, libpython3-dev, graphviz,
        git, build-essential, pkg-config, myrepos,
        python3-psycopg2, postgresql-client
      ]
  - name: install pip dependencies
    pip:
      name: [
        # Cassandra Python client
        cassandra-driver, lz4,
        # benchmark script
        click,
      ]
      executable: pip3


- hosts: clients
  tasks:
  - name: copy swh-environment
    synchronize:
      src: /home/dev/swh-environment/
      dest: /home/vlorentz/swh-environment/
      delete: yes
      rsync_opts: ["--exclude", "*/__pycache__/", "--exclude", "*/.*"]
  - block:
    - name: install swh-environment
      shell: "pip3 install $(./bin/pip-swh-packages) --user"
      args:
        chdir: /home/vlorentz/swh-environment
        creates: /home/vlorentz/.local/lib/python*/site-packages/swh.storage.egg-link
    rescue:
    - name: install swh-environment
      # Can't use /usr/bin/pip3 if there is a newer pip lib installed locally
      shell: "~/.local/bin/pip3 install $(./bin/pip-swh-packages) --user"
      args:
        chdir: /home/vlorentz/swh-environment

  - name: copy scripts
    copy:
      src: scripts/
      dest: /home/vlorentz/scripts/
      mode: a+x

  - name: set env variables in bashrc
    blockinfile:
      path: /home/vlorentz/.bashrc
      block: |
        export SWH_BENCH_CASSANDRA_HOSTS=\
        "{% for server in groups['cassandra-servers'] %}{{hostvars[server].ansible_host}} {% endfor %}"

        {% if groups['pgsql-servers'] %}
        export SWH_BENCH_PGSQL_HOST={{hostvars[groups['pgsql-servers'][0]].ansible_host}}
        {% endif %}

  - name: wait for Cassandra to start
    wait_for:
      host: "{{hostvars[item].ansible_host}}"
      port: 9042
    with_items: "{{groups['cassandra-servers']}}"

  - name: initialize Cassandra db
    shell: >
      /home/vlorentz/scripts/cassandra-bench-tools.py cassandra
      {% for server in groups['cassandra-servers'] %}
        --host {{hostvars[server].ansible_host}}
      {%- endfor %}
      init-db
    args:
      executable: bash
