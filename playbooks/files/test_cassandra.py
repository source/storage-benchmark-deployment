#!/usr/bin/env python3

import sys

from cassandra.cluster import Cluster
from cassandra.policies import RoundRobinPolicy

if len(sys.argv) != 2:
    print('Syntax: test_cassandra.py <host1,host2,host3>', file=sys.stderr)
    exit(1)
hosts = sys.argv[1].split(',')

cluster = Cluster(
    hosts,
    load_balancing_policy=RoundRobinPolicy())

session = cluster.connect()
