#!/bin/bash

DIR="$( dirname "${BASH_SOURCE[0]}" )"
NB_CPUS="$(cat /proc/cpuinfo | grep -i "^processor" | wc -l)"
NB_JOBS="$(expr $NB_CPUS "*" 4)"

cat /mnt/dataset/revision.csv.gz \
    | pigz -d \
    | pv --line-mode --average-rate --rate --timer --bytes \
    | parallel -j $NB_JOBS --round-robin --pipe --line-buffer \
        python3 $DIR/cassandra-bench-tools.py pgsql import-revision-dataset

