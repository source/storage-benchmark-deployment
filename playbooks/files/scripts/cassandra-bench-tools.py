#!/usr/bin/env python3
import csv
import gc
import gzip
import os
import re
import sys
import time
import datetime

from cassandra.cluster import Cluster
from cassandra.policies import RoundRobinPolicy
import click
import dateutil.parser

from swh.model.identifiers import normalize_timestamp
from swh.model.hashutil import hash_to_bytes

from swh.storage import get_storage

CREATE_DB_QUERIES = [
    '''
CREATE TYPE IF NOT EXISTS microtimestamp (
    seconds             bigint,
    microseconds        int
);
''',
    '''
CREATE TYPE IF NOT EXISTS microtimestamp_with_timezone (
    timestamp           frozen<microtimestamp>,
    offset              smallint,
    negative_utc        boolean
);
''',
    '''
CREATE TYPE IF NOT EXISTS person (
    fullname    blob,
    name        blob,
    email       blob
);
''',
    '''
CREATE TABLE IF NOT EXISTS revision (
    id                              blob PRIMARY KEY,
    date                            microtimestamp_with_timezone,
    committer_date                  microtimestamp_with_timezone,
    type                            ascii,
    directory                       blob,  -- source code "root" directory
    message                         blob,
    author                          person,
    committer                       person,
    parents                         frozen<list<blob>>,
    synthetic                       boolean,
        -- true iff revision has been created by Software Heritage
    metadata                        text
        -- extra metadata as JSON(tarball checksums,
        -- extra commit information, etc...)
);
'''
]

REVISION_FILE_COLS = (
    'id', 'date', 'date_offset', 'committer_date', 'committer_date_offset',
    'type', 'directory', 'message', 'author', 'committer', 'metadata')

REVISION_CASSANDRA_COLS = ['id'] + sorted([
    'date', 'committer_date', 'directory', 'type', 'message',
    'author', 'committer', 'parents', 'synthetic', 'metadata'])


def bytea_to_blob(bytea):
    """pg hex notation to cassandra's."""
    return '0' + bytea[1:]

def bytea_to_bytes(bytea):
    """pg hex notation to bytes."""
    return hash_to_bytes(bytea[2:])

_time_re = re.compile(r'(\d{4})-(\d{2})-(\d{2}).(\d{2}):(\d{2}):(\d{2})\+\d+')

def parse_dt(s, offset):
    dt = datetime.datetime(*map(int, _time_re.match(s).groups())) \
        .replace(tzinfo=datetime.timezone.utc)
    seconds = int(dt.timestamp())
    return {
        'timestamp': {
            'seconds': int(dt.timestamp()),
            'microseconds': dt.microsecond,
        },
        'offset': int(offset),
        'negative_utc': offset[0] == '-' and int(offset) == 0,
    }


def push_revisions(path, storage):
    start = time.time()
    with open(path) as in_file:
        reader = csv.reader(in_file)
        writer = csv.writer(sys.stdout)
        batch = []
        nb_added = 0
        for (n, row) in enumerate(reader):
            rev = dict(zip(REVISION_FILE_COLS, row))

            # TODO: time offsets
            rev = {
                'id': bytea_to_bytes(rev['id']),
                'directory': bytea_to_bytes(rev['directory']),
                'message': bytea_to_bytes(rev['message']),
                'date': parse_dt(rev['date'], rev['date_offset']),
                'committer_date': parse_dt(rev['committer_date'], rev['committer_date_offset']),
                'author': {
                    'fullname': b'',
                    'name': b'',
                    'email': b'',
                },
                'committer': {
                    'fullname': b'',
                    'name': b'',
                    'email': b'',
                },
                'metadata': None,
                'parents': [],
                'synthetic': False,
                'type': rev['type'],
            }
            batch.append(rev)
            if len(batch) % 1000 == 0:
                #stats = storage.revision_add(batch, check_missing=False)
                stats = storage.revision_add(batch)
                nb_added += stats['revision:add']
                batch = []
            '''
            if n and n % 100 == 0:
                print(('read %d rows (avg: %d rows/s), '
                       'wrote %d (avg: %d rows/s)') %
                      (n, int(n/(time.time()-start)),
                       nb_added, int(nb_added/(time.time()-start))),
                      file=sys.stderr)'''


def print_revisions_pretty(path):
    start = time.time()
    with open(path) as in_file:
        reader = csv.reader(in_file)
        writer = csv.writer(sys.stdout)
        for (n, row) in enumerate(reader):
            rev = dict(zip(REVISION_FILE_COLS, row))

            # TODO: time offsets
            rev = {
                **rev,
                'id': bytea_to_blob(rev['id']),
                'directory': bytea_to_blob(rev['directory']),
                'message': bytea_to_blob(rev['message']),
                'date': parse_dt(rev['date'], rev['date_offset']),
                'committer_date': parse_dt(rev['committer_date'], rev['committer_date_offset']),
                'author': None,
                'committer': None,
                'metadata': None,
                'parents': None,
                'synthetic': False,
            }

            row = [rev[key] for key in REVISION_CASSANDRA_COLS]
            writer.writerow(row)
            if n and n % 10000 == 0:
                while n/(time.time()-start) > 10000:
                    time.sleep(0.1)
                print('wrote %d rows (average: %d rows/s).' %
                      (n, int(n/(time.time()-start))),
                      file=sys.stderr)

def print_revisions_ugly(path):
    start = time.time()
    with open(path) as in_file:
        reader = csv.reader(in_file)
        writer = csv.writer(sys.stdout)
        for (n, row) in enumerate(reader):
            row2 = (
                bytea_to_blob(row[0]),  # id
                None, # author
                None, # committer
                parse_dt(row[3], row[4]), # committer_date
                parse_dt(row[1], row[2]), # date
                bytea_to_blob(row[6]), # directory
                bytea_to_blob(row[7]), # message
                None, # metadata
                None, # parents
                False, # synthetic
                row[5], # type
            )
            writer.writerow(row2)
            if n and n % 1000 == 0:
                print('wrote %d rows (average: %d rows/s).' %
                      (n, int(n/(time.time()-start))),
                      file=sys.stderr)

@click.group()
@click.pass_context
def cli(ctx):
    ctx.ensure_object(dict)

@cli.group('cassandra')
@click.option('--host', 'hosts', multiple=True, 
              help='Cassandra hosts')
@click.pass_context
def cassandra(ctx, hosts):
    ctx.obj['hosts'] = hosts

@cassandra.command('init-db')
@click.pass_context
def cassandra_init_db(ctx):
    cluster = Cluster(
        ctx.obj['hosts'],
        load_balancing_policy=RoundRobinPolicy())
    session = cluster.connect()
    session.execute(
        '''CREATE KEYSPACE IF NOT EXISTS test_perf
           WITH REPLICATION = {
                'class' : 'SimpleStrategy',
                'replication_factor' : 1
           };''')
    session.execute('USE test_perf;')

    for query in CREATE_DB_QUERIES:
        session.execute(query)

    print('Cassandra DB initialized.')

@cassandra.command('import-revision-dataset')
@click.pass_context
def cassandra_import_revision_dataset(ctx):
    storage = get_storage('cassandra', {
            'hosts': ctx.obj['hosts'],
            'keyspace': 'test_perf',
        })
    push_revisions('/dev/stdin', storage)

@cli.group('pgsql')
@click.option('--host',
              help='pgsql host')
@click.pass_context
def pgsql(ctx, host):
    ctx.obj['host'] = host

def get_pg_storage(ctx):
    return get_storage('local', {
        'db': 'dbname=softwareheritage-test user=swh password=testpassword host=%s' % ctx.obj['host'],
        'objstorage': {
            'cls': 'remote',
            'args': {
                'url': 'foo',
            },
        },
    })

@pgsql.command('import-revision-dataset')
@click.pass_context
def pgsql_import_dataset(ctx):
    storage = get_pg_storage(ctx)
    push_revisions('/dev/stdin', storage)


if __name__ == '__main__':
    csv.field_size_limit(sys.maxsize)
    cli(auto_envvar_prefix='SWH_BENCH')
