variable "nb_cassandra_servers" {
  type = "string"
  default = 4
}
variable "nb_pgsql_servers" {
  type = "string"
  default = 1
}
variable "nb_clients" {
  type = "string"
  default = 1
}

variable "ssh_key_data" {
  type = "string"
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC1gRhavvtL92IIuXdVujwVnptT2aDlJafaDNrr7AVo2Eboc7MAlQC+9UHDpMqVhmLL3J9OFR7vSFMYjsRoREIB5wxsoTOQy5D2kZwbu28dMy7ZEM+gNFyQaGucngTSj0d3m80RVBYOMDupaXtPuw0fEzRKmHMOr3zMhCK8djbdMKIWnPMA2mxGjiCY22jd7k3jlGti3iXjZ+Mrl1VGKuOXiEpHsDNoQfL289odkP4nWQA95tS6Lwi2gUtSLmhu9QTfIoFVFwdGT8jLa3ql3Ia45otCDTH2SrI9ZYoUhmTDNwcVhhkpfCb8tbsU0zD6qSTn5EWHpv6+56oa3nLutgF1 azure@desktop5"
}
